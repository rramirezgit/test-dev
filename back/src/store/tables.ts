import { SessionModel } from "./models";

export const getModels = (model: string) => {
  const models: any = {
    session: SessionModel,
  };
  return models[model];
};
