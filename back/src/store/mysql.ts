import { Sequelize, FindOptions, QueryTypes } from "sequelize";
import { getModels } from "./tables";
import config from "../config";

const { DB_DATABASE, DB_HOST, DB_PASSWORD, DB_USER } = config.database.mysql;

const connection = new Sequelize(DB_DATABASE, DB_USER, DB_PASSWORD, {
  host: DB_HOST,
  dialect: "mysql",
  logging: false,
});

const options: FindOptions = {
  raw: true,
  nest: true,
};

const queryOptions = {
  timestamps: false,
};

function testConnection() {
  connection
    .authenticate()
    .then(() => {
      console.log("Connection has been established successfully.");
    })
    .catch((err) => {
      console.error("Unable to connect to the database:", err);
    });
}

async function list<T = any>(table: string) {
  const myModelSchema = getModels(table);
  //@ts-ignore
  return (await connection
    .define(table, myModelSchema, queryOptions)
    .findAll(options)) as T;
}
async function getById<T = any>(table: string, filterOptions?: any) {
  const myModelSchema = getModels(table);

  const customFilter: FindOptions = {
    where: {
      ...filterOptions,
    },
  };

  const myModel = connection.define(table, myModelSchema, queryOptions);
  if (Object.keys(myModelSchema || {}).includes("id")) {
    myModel.removeAttribute("id");
  }

  //@ts-ignore
  return (await myModel.findOne({
    ...customFilter,
    ...options,
  })) as T;
}

async function insert<T = any>(table: string, data: T) {
  const myModelSchema = getModels(table);
  const myModel = connection.define(table, myModelSchema, queryOptions);
  if (Object.keys(myModelSchema || {}).includes("id")) {
    myModel.removeAttribute("id");
  }

  return await myModel.create({ ...data }).then((data) => {
    return data;
  });
}

async function upsert<T = any>(table: string, data: any, options?: any) {
  const myModelSchema = getModels(table);
  const myModel = connection.define(table, myModelSchema, queryOptions);
  if (Object.keys(myModelSchema || {}).includes("id")) {
    myModel.removeAttribute("id");
  }

  return await myModel.upsert({ ...data }, { ...options });
}

async function update(table: string, condition: any, data: any) {
  const myModelSchema = getModels(table);
  const myModel = connection.define(table, myModelSchema, queryOptions);
  if (Object.keys(myModelSchema || {}).includes("id")) {
    myModel.removeAttribute("id");
  }
  return await myModel.update(
    { ...data },
    {
      where: {
        ...condition,
      },
    }
  );
}

async function remove(table: string, condition?: any) {
  const myModelSchema = getModels(table);
  const myModel = connection.define(table, myModelSchema, queryOptions);
  if (Object.keys(myModelSchema || {}).includes("id")) {
    myModel.removeAttribute("id");
  }

  return await myModel.destroy({
    where: {
      ...condition,
    },
  });
}

async function query<T = any>(sqlQuery: string) {
  return (await connection.query(sqlQuery, {
    ...queryOptions,
    ...options,
  })) as Array<T>;
}
export default {
  testConnection,
  list,
  getById,
  insert,
  update,
  remove,
  query,
  upsert
};
