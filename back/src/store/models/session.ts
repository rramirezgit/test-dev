import { DataTypes, Model, Optional } from "sequelize";

export interface ISession {
  session_id: string;
  expires: number ;
  data: string;
}

export const SessionModel = {
  session_id: { type: DataTypes.STRING, primaryKey: true },
  expires: DataTypes.NUMBER,
  data: DataTypes.TEXT,
};
