import { Response, Request, NextFunction } from "express";
import { EventEmitter } from "events";
import Cookie, { CookieProps } from "./session/cookie";
import Session, { SessionProps } from "./session/session";
import Stored from "./session/store";
import { Repository } from "./store/repostiroy";
import parseUrl from "parseurl";
import cookie, { CookieSerializeOptions } from "cookie";
import signature from "cookie-signature";
import onHeaders from "on-headers";
import crypto from "crypto";
var Buffer = require("safe-buffer").Buffer;

const uid = require("uid-safe").sync;
interface Store extends Repository, Stored {}
export interface RequestSession extends Request, SessionProps {
  sessionID: any;
  cookie: Cookie;
  sessionStore?: Store;
  session?: Session;
}

interface sessionOptions {
  secret?: string;
  cookieName?: string;
  cookie?: CookieProps;
  store?: Store;
}

export function session(options: sessionOptions) {
  const opts = options || {};
  const secret: Array<string> = opts?.secret ? [opts?.secret] : ["mysecret"];
  const cookieOptions = opts?.cookie;
  const name = opts?.cookieName || "sessionId";
  const store = opts?.store;

  //generate the new session
  const generateNewSession = (req: RequestSession) => {
    req.sessionID = generateId();
    req.session = new Session(req);
    req.session.cookie = new Cookie(cookieOptions);
  };

  // register event listeners for the store to track readiness
  var storeReady = true;
  store?.on("disconnect", function ondisconnect() {
    storeReady = false;
  });
  store?.on("connect", function onconnect() {
    storeReady = true;
  });

  return function (req: RequestSession, resp: Response, next: NextFunction) {
    if (req.session) {
      next();
    }

    if (!storeReady) {
      next();
    }
    // pathname mismatch
    const originalPath = parseUrl.original(req)?.pathname || "/";
    if (originalPath.indexOf(cookieOptions?.path || "/") !== 0) {
      next();
    }

    if (!secret) {
      next(new Error("secret option is required"));
    }

    req.sessionStore = store;

    let originalId: string;
    let originalHash: string;
    let savedHash: string;

    const cookieId = getCookie(req, name, secret);
    req.sessionID = cookieId;
    let touched = false;

    onHeaders(resp, () => {
      if (!req.session) {
        return;
      }
      if (!shouldSetCookie(req, cookieId)) {
        return;
      }

      if (!touched) {
        req.session.touch();
        touched = true;
      }
      //set cookie
      setCookie(
        resp,
        name,
        req?.sessionID,
        secret,
        req.session.cookie.getData()
      );
    });

    var _end = resp.end;
    var _write = resp.write;
    var ended = false;
    //@ts-ignorek
    resp.end = function end(chunk, encoding) {
      if (ended) {
        return false;
      }

      ended = true;
      //@ts-ignore
      var ret;
      var sync = true;

      function writeend() {
        if (sync) {
          //@ts-ignore
          ret = _end.call(resp, chunk, encoding);
          sync = false;
          return;
        }
        //@ts-ignore
        _end.call(resp);
      }

      function writetop() {
        if (!sync) {
          //@ts-ignore
          return ret;
        }

        if (chunk == null) {
          ret = true;
          return ret;
        }

        var contentLength = Number(resp.getHeader("Content-Length"));

        if (!isNaN(contentLength) && contentLength > 0) {
          // measure chunk
          chunk = !Buffer.isBuffer(chunk)
            ? Buffer.from(chunk, encoding)
            : chunk;
          encoding = undefined;

          if (chunk.length !== 0) {
            //@ts-ignore
            ret = _write.call(resp, chunk.slice(0, chunk.length - 1));
            chunk = chunk.slice(chunk.length - 1, chunk.length);
            return ret;
          }
        }
        //@ts-ignore
        ret = _write.call(resp, chunk, encoding);
        sync = false;

        return ret;
      }
      // no session to save
      if (!req.session) {
        //@ts-ignore
        return _end.call(resp, chunk, encoding);
      }

      if (!touched) {
        // touch session
        req.session.touch();
        touched = true;
      }

      if (shouldSave(req, cookieId, originalId, originalHash)) {
        //@ts-ignore
        req.session.save(function onsave(err) {
          if (err) {
            //@ts-ignore
            defer(next, err);
          }

          writeend();
        });

        return writetop();
      } else if (shouldTouch(req, cookieId, originalId, originalHash)) {
        // store implements touch method
        //@ts-ignore
        store.touch(req.sessionID, req.session, function ontouch(err) {
          if (err) {
            //@ts-ignore
            defer(next, err);
          }

          writeend();
        });

        return writetop();
      }
      //@ts-ignore
      return _end.call(resp, chunk, encoding);
    };

    function generateSession(req: RequestSession) {
      generateNewSession(req);
      originalId = req.sessionID;
      originalHash = hash(req.session);
      wrapSessionmethods(req.session, savedHash);
    }

    if (!req.sessionID) {
      generateSession(req);
      next();
      return;
    }
    function inflateSession(req: RequestSession, session: any, store: Store) {
      store.createSession(req, session);
      originalId = req.sessionID;
      originalHash = hash(session);
      wrapSessionmethods(req.session, savedHash);
    }

    store?.get(req.sessionID, (err: any, session: any) => {
      if (err && err.code !== "ENOENT") {
        next(err);
      }

      try {
        inflateSession(req, session, store);
      } catch (error) {
        next(error);
        return;
      }
      next();
    });
  };
}
//generate UID
const generateId = () => {
  return uid(24);
};
//Set cookie on response.
const setCookie = (
  resp: Response,
  name: string,
  sessionID: any,
  secret: string[],
  options: any
) => {
  const signed = "s:".concat(signature.sign(sessionID, secret[0]));
  const data = cookie.serialize(name, signed, options) as string;
  const prev = resp.getHeader("Set-Cookie") || [];
  const header = Array.isArray(prev) ? prev.concat(data) : [prev, data];

  resp.setHeader("Set-Cookie", header as Array<string>);
};
// Get the session ID cookie from request
const getCookie = (
  req: RequestSession,
  name: string,
  secrets: Array<string>
): string | undefined => {
  const header = req.headers?.cookie;

  let raw;
  let val: string | undefined;
  if (header) {
    const cookies = cookie.parse(header);
    raw = cookies[name];

    //signed
    if (raw?.substr(0, 2) === "s:") {
      val = unsigncookie(raw.slice(2), secrets);
    }
  }

  return val;
};
//Verify and decode the given `val` with `secrets`
function unsigncookie(val: string, secrets: string[]): string | undefined {
  for (var i = 0; i < secrets.length; i++) {
    var result = signature.unsign(val, secrets[i]);

    if (result !== false) {
      return result;
    }
  }

  return undefined;
}

//determine if cookie should be set on response.
function shouldSetCookie(
  req: RequestSession,
  cookieId: string | undefined
): boolean {
  return cookieId !== req?.sessionID;
}

// determine if session should be saved to store
function shouldSave(
  req: RequestSession,
  cookieId: string | undefined,
  originalId: string,
  originalHash: string
): boolean {
  return cookieId !== req.sessionID
    ? isModified(req.session, originalId, originalHash)
    : !isSaved(req.session, originalId, originalHash);
}
// check if session has been saved
function isSaved(
  session: RequestSession["session"],
  originalId: String,
  saveHash: string
): boolean {
  return originalId === session?.id && saveHash === hash(session);
}
// check if session has been modified
function isModified(
  session: RequestSession["session"],
  originalId: string,
  originalHash: string
): boolean {
  return originalId !== session?.id || originalHash !== hash(session);
}

// determine if session should be touched
function shouldTouch(
  req: RequestSession,
  cookieId: string | undefined,
  originalId: string,
  originalHash: string
): boolean {
  return (
    req.sessionID === cookieId &&
    !shouldSave(req, cookieId, originalId, originalHash)
  );
}
// wrap session methods
function wrapSessionmethods(
  session: RequestSession["session"] | undefined,
  savedHash: string
) {
  let _save = session?.save;
  function save() {
    savedHash = hash(session);
    //@ts-ignore
    _save?.apply(session, arguments);
  }

  Object.defineProperty(session, "save", {
    configurable: true,
    enumerable: false,
    value: save,
    writable: true,
  });
}

function hash(session: RequestSession["session"]): string {
  var str = JSON.stringify(session, function (key, val) {
    // ignore sess.cookie property
    if (this === session && key === "cookie") {
      return;
    }
    return val;
  });
  return crypto.createHash("sha1").update(str, "utf8").digest("hex");
}

// ignore next
const defer =
  typeof setImmediate === "function"
    ? setImmediate
    : function (fn: any, args: any) {
        process.nextTick(fn.bind.apply(fn, args));
      };

export { session as default };
