import { clearInterval } from "timers";
import dbStore from "../../../store";
import { ISession } from "../../../store/models";
import { Repository } from "./repostiroy";
import { CookieProps } from "../session/cookie";
import session, { RequestSession } from "..";
import Session from "../session/session";
const StoreSession = require("./../session/store");
const Utils = require("util");

const TABLE = "session";
interface SchemaProps {
  tableName: string;
  columnNames: {
    session_id: string;
    expires: string;
    data: string;
  };
}
export interface OptionsProps {
  clearExpired?: boolean;
  checkExpirationInterval?: number;
  ttl?: number;
  createDatabaseTable?: boolean;
}

class DatabaseStore implements Repository {
  defaultOptions: OptionsProps = {
    clearExpired: false,
    ttl: 86400000,
    checkExpirationInterval: 10 * 1000,
  };
  options: OptionsProps = this.defaultOptions;
  callBack: any;
  _expirationInterval: any;
  constructor(options: OptionsProps) {
    //this.callBack = callBack;
    this.setOptions(options);
    if (this.options.clearExpired && this.options.checkExpirationInterval) {
      this.setExpirationInterval(this.options.checkExpirationInterval);
    }
    Utils.inherits(DatabaseStore, StoreSession);
  }

  private setOptions(options?: OptionsProps) {
    this.options = {
      ...this.defaultOptions,
      ...options,
    };
  }

  get(session_id: RequestSession["sessionID"], callBack: any) {
    //get session
    const filter = {
      session_id,
    };
    dbStore.getById<ISession>(TABLE, filter).then((resp) => {
      if (!resp) {
        return callBack(null, null);
      }
      //check expire time
      const now = Math.round(Date.now() / 1000);
      if (resp.expires < now) {
        //session has expired.
        return callBack(null, null);
      }
      try {
        const session = JSON.parse(resp.data);
        callBack(null, session);
      } catch (err) {
        return callBack(err);
      }
    });
  }
  set(
    session_id: RequestSession["sessionID"],
    data: Session | undefined,
    callBack: any
  ) {
    //convert data to string
    const cookieData = JSON.stringify(data);
    const expiresSeconds = this.getExpires(data);

    const dataInsert = {
      session_id,
      expires: expiresSeconds,
      data: cookieData,
    };

    dbStore
      .upsert(TABLE, dataInsert, { expires: "expires", data: "data" })
      .then(() => callBack())
      .catch((err) => callBack(err));
  }

  touch(
    session_id: RequestSession["sessionID"],
    data: Session | undefined,
    callBack: any
  ) {
    const expiresSeconds = this.getExpires(data);
    const where = {
      session_id,
    };
    const dataUpdate = {
      expires: expiresSeconds,
    };
    dbStore
      .update(TABLE, where, dataUpdate)
      .then(() => callBack())
      .catch((err) => callBack(err));
  }
  destroy(session_id: RequestSession["sessionID"], callBack: any) {
    const where = {
      session_id,
    };
    dbStore
      .remove(TABLE, where)
      .then(() => callBack())
      .catch((err) => callBack(err));
  }

  clear(callBack: any) {
    dbStore
      .remove(TABLE)
      .then(() => callBack())
      .catch((err) => callBack(err));
  }
  private setExpirationInterval(interval: number) {
    const intervalMs = interval
      ? interval
      : this.options.checkExpirationInterval;

    this.clearExpirationInterval();
    this._expirationInterval = setInterval(
      this.clearExpiredSessions,
      intervalMs
    );
  }
  private clearExpirationInterval() {
    clearInterval(this._expirationInterval);
    this._expirationInterval = null;
  }
  private clearExpiredSessions() {
    const nowSeconds = Math.round(Date.now() / 1000);
    const where = {
      expires: nowSeconds,
    };
    dbStore.remove(TABLE, where).then(console.log).catch(console.log);
  }
  private close() {
    this.clearExpirationInterval();
  }

  private getExpires(data: Session | undefined) {
    let expires;
    if (data?.cookie) {
      if (data?.cookie.expires) {
        expires = data?.cookie.expires;
      } else if (data?.cookie._expires) {
        expires = data?.cookie._expires;
      }
    }

    if (!expires) {
      const expiration = this.options?.ttl || 0;
      expires = Date.now() + expiration;
    }

    if (!(expires instanceof Date)) {
      expires = new Date(expires);
    }

    return Math.round(expires.getTime() / 1000);
  }
}

export { DatabaseStore as default };

module.exports = DatabaseStore;
