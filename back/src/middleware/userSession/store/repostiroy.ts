import Session from "../session/session";
import { RequestSession } from "..";


export interface Repository {
  get(
    session_id: RequestSession["sessionID"],
    callBack: any
  ): void;
  set(
    session_id: RequestSession["sessionID"],
    data: Session | undefined,
    callBack: any
  ): void;
  touch(
    session_id: RequestSession["sessionID"],
    data: Session | undefined,
    callBack: any
  ): void;
  clear(callBack: any): void;

  destroy(
    session_id: RequestSession["sessionID"],
    data: Session | undefined,
    callBack: any
  ): void;
}
