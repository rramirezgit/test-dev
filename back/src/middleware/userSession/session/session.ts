import { RequestSession } from "..";
import Cookie, { CookieProps } from "./cookie";
export interface SessionProps {
  touch(): void;
  resetMaxAge(): void;
  save(fn: any): void;
}
class Session implements SessionProps {
  //@ts-ignore
  req: RequestSession;
  id: any;
  cookie: Cookie = new Cookie();

  constructor(req: RequestSession, data?: any) {
    Object.defineProperty(this, "req", { value: req });
    Object.defineProperty(this, "id", { value: req.sessionID });

    if (typeof data === "object" && data !== null) {
      for (const key in data) {
        //@ts-ignore
        if (!(key in this)) this[key] = data[key];
      }
    }

    this.defineMethod(this, "touch", this.touch);
    this.defineMethod(this, "save", this.save);
    this.defineMethod(this, "resetMaxAge", this.resetMaxAge);
  }

  touch() {
    this.resetMaxAge();
  }

  resetMaxAge() {
    this.cookie.maxAge = this.cookie.originalMaxAge;
  }

  save(fn: any) {
    this.req.sessionStore?.set(this.id, this, fn || function () {});
  }

  private defineMethod(obj: any, name: string, fn: Function) {
    Object.defineProperty(obj, name, {
      configurable: true,
      enumerable: false,
      value: fn,
      writable: true,
    });
  }
}

module.exports = Session;

export { Session as default };
