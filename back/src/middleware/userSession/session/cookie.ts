import cookie from "cookie";

export interface CookieProps {
  path: string;
  maxAge: number;
  originalMaxAge: number;
  _expires: any;
  expires: any;
  httpOnly: boolean;
  secure?: any;
  domain?: string;
}

class Cookie implements CookieProps {
  path = "/";
  maxAge = 1000;
  originalMaxAge = 1000;
  _expires: any;
  expires: any;
  httpOnly = true;

  constructor(options?: CookieProps) {
    if (options && typeof options === "object") {
      for (const key in options) {
        if (key !== "data") {
          //@ts-ignore
          this[key] = options[key];
        }
      }
    }
    if (!this.originalMaxAge) {
      this.originalMaxAge = this.maxAge;
    }
  }
  //getters and setters
  setExpires(date: Date) {
    this._expires = date;
    this.originalMaxAge = this.maxAge;
  }

  getExpires() {
    return this._expires;
  }

  setMaxAge(ms: any) {
    this.expires = typeof ms === "number" ? new Date(Date.now() + ms) : ms;
  }

  getMaxAge() {
    return this.expires instanceof Date
      ? this.expires.valueOf() - Date.now()
      : this.expires;
  }

  getData() {
    const { _expires, expires, ...allParams } = this;
    return {
      ...allParams,
      expires: _expires,
    };
  }

  serialize(name: string, val: string) {
    return cookie.serialize(name, val, this.getData());
  }

  toJSON() {
    return this.getData();
  }
}

module.exports = Cookie;
export { Cookie as default };
