import { RequestSession } from "..";
import { Repository } from "../store/repostiroy";
import Session from "./session"

const Cookie = require("./cookie");
const USesion = require("./session");

import { EventEmitter } from "events";

interface StoreProps {
  createSession(req:RequestSession,session:Session): void;
}
class Store extends EventEmitter implements StoreProps {
  createSession(req: RequestSession, session: Session) {    
    const expires = session.cookie.expires;
    const originalMaxAge = session.cookie.originalMaxAge;
    session.cookie = new Cookie(session.cookie);
    if (typeof expires === "string") {
      session.cookie.expires = new Date(expires);
    }

    session.cookie.originalMaxAge = originalMaxAge;
    req.session = new USesion(req, session);
    return req.session;
  }
}
export { Store as default };
module.exports = Store;
