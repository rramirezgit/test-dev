import { config } from "dotenv";
import path from "path";

config({
  path:path.join(path.dirname(__dirname),".env"),
});

export default {
  services: {
    api: {
      PORT: process.env.PORT || 3000,
    },
  },
  session: {
    secret: process.env.SESSION_SECRET_KEY || "mysecret",
    cookieName: process.env.SESSION_COOKIENAME || "sessionId",
    ttl: process.env.SESSION_TTL || 900,
    sessionDefaultId: process.env.SESSION_DEFAULT_ID || 3600,
  },
  database: {
    mysql: {
      DB_HOST: process.env.DB_MYSQL_HOST || "",
      DB_USER: process.env.DB_MYSQL_USER || "",
      DB_PASSWORD: process.env.DB_MYSQL_PASSWORD || "",
      DB_DATABASE: process.env.DB_MYSQL_DATABASE || "",
    },
  },
};
