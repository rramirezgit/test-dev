import express, { Request, Response } from "express";
import config from "./config";
import { RequestSession } from "./middleware/userSession";
import session from "./middleware/userSession";
import SessionStoreDb,{OptionsProps} from "./middleware/userSession/store/database"


const app = express();

const PORT = config.services.api.PORT;

var options:OptionsProps = {
  clearExpired:true,
};

var sessionStore = new SessionStoreDb(options);

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, resp, next) => {
  session({
    cookieName: config.session.cookieName,
    secret: config.session.secret,
    //@ts-ignore
    store: sessionStore,
  })(req as RequestSession, resp, next);
});

app.get("/", (req: Request, resp: Response) => {
  //@ts-ignore
  req.session.view = req.session.view ? ++req.session.view : 1;
  //@ts-ignore
  resp.send(`Views:[${req.session.view}]`);
});

app.listen(PORT, () => {
  console.log(`App run in http://localhost:${PORT}`);
});
