#Requerimientos
- docker
- docker-compose

## Desplegar en local
###### Paso 1
> Copiar las variables de entorno ubicadas en /back

`mv .env.example .env`

###### paso 2
>Ejecutar docker en /stack

`docker-compose up -d --build 
`
######  Verificar si funcion a en las siguientes URL:
|  TYPE |  URL  |
| ------------ | ------------ |
| API | http://loclahost:3000 |
| DB-MONITOR | http://localhost:8080 |

######  Credenciales de la Base de Datos:
|  TYPE |  URL  |
| ------------ | ------------ |
| USER | test |
| PASS | root |
| DB | test |
